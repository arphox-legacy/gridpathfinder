﻿using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace GridPathFinder.Tests
{
    [TestFixture]
    public sealed class TileDistanceCalculatorTests
    {
        [Test]
        public void CalculateTileDistances_WhenCalledForSecondTime_ThrowsException()
        {
            // Arrange
            var tiles = new List<Tile>()
            {
                new Tile(0, 0, TileType.Self),
                new Tile(0, 1, TileType.Target)
            };

            var calculator = new TileDistanceCalculator(tiles);
            calculator.CalculateTileDistances();

            // Act
            Action secondCall = () => calculator.CalculateTileDistances();

            // Assert
            Assert.Throws<InvalidOperationException>(() => secondCall());
        }
    }
}