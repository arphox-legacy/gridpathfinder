﻿using GridPathFinder.Utilities;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace GridPathFinder.Tests.UtilitiesTests
{
    [TestFixture]
    public sealed class StringExtensions_To2DCharArray_Tests
    {
        [Test]
        public void ForNullInput_ThrowsArgumentNullException()
        {
            // Arrange
            string input = null;

            // Act (delayed)
            Action action1 = () => input.To2DCharArray();
            Action action2 = () => input.To2DCharArray(new string[] { "separator" });

            // Assert
            Assert.Multiple(() =>
            {
                Assert.Throws<ArgumentNullException>(() => action1());
                Assert.Throws<ArgumentNullException>(() => action2());
            });
        }

        [Test]
        public void ForEmptyInput_ThrowsArgumentException()
        {
            // Arrange
            string input1 = "";
            string input2 = string.Empty;

            // Act (delayed)
            Action action1 = () => input1.To2DCharArray();
            Action action2 = () => input2.To2DCharArray(new string[] { "separator" });

            // Assert
            Assert.Multiple(() =>
            {
                Assert.Throws<ArgumentException>(() => action1());
                Assert.Throws<ArgumentException>(() => action2());
            });
        }

        [Test]
        public void ForNullSeparators_ThrowsArgumentNullException()
        {
            // Arrange
            string input = "asd";
            string[] separators = null;

            // Act (delayed)
            Action action = () => input.To2DCharArray(separators);

            // Assert
            Assert.Throws<ArgumentNullException>(() => action());
        }

        [Test]
        public void ForEmptySeparators_ThrowsArgumentException()
        {
            // Arrange
            string input = "asd";
            string[] separators = new string[] { };

            // Act (delayed)
            Action action = () => input.To2DCharArray(separators);

            // Assert
            Assert.Throws<ArgumentException>(() => action());
        }

        [TestCase("aa_bbb_cc")]
        [TestCase("aa_bb_ccc")]
        [TestCase("aaa__ccc_a")]
        [TestCase("aasdasdsad_vvva")]
        public void ForMalformedInput_ThrowsArgumentException(string input)
        {
            // Arrange
            string[] separators = new string[] { "_" };

            // Act (delayed)
            Action action = () => input.To2DCharArray(separators);

            // Assert
            Assert.Throws<ArgumentException>(() => action());
        }

        [TestCaseSource(nameof(CreateStructureCheckTestCases))]
        public void CreatesArrayCorrectly(string input, char[,] expected)
        {
            // Arrange
            string[] separators = new string[] { " " };

            // Act (delayed)
            char[,] actual = input.To2DCharArray(separators);

            // Assert
            Assert.That(actual.Rank, Is.EqualTo(expected.Rank));
            Assert.That(actual.GetLength(0), Is.EqualTo(expected.GetLength(0)));
            Assert.That(actual.GetLength(1), Is.EqualTo(expected.GetLength(1)));
            Assert.That(actual.Length, Is.EqualTo(expected.Length));

            for (int i = 0; i < expected.GetLength(0); i++)
            {
                for (int j = 0; j < expected.GetLength(1); j++)
                {
                    Assert.That(actual[i, j], Is.EqualTo(expected[i, j]));
                }
            }
        }

        [Test]
        public void CreatesArrayCorrectly_NoSeparatorsArray()
        {
            // Arrange
            string input = "abc" + Environment.NewLine +
                           "def";

            // Act (delayed)
            char[,] actual = input.To2DCharArray();

            // Assert
            Assert.That(actual.Rank, Is.EqualTo(2));
            Assert.That(actual.GetLength(0), Is.EqualTo(2));
            Assert.That(actual.GetLength(1), Is.EqualTo(3));
            Assert.That(actual.Length, Is.EqualTo(2 * 3));

            Assert.That(actual[0, 0], Is.EqualTo('a'));
            Assert.That(actual[0, 1], Is.EqualTo('b'));
            Assert.That(actual[0, 2], Is.EqualTo('c'));

            Assert.That(actual[1, 0], Is.EqualTo('d'));
            Assert.That(actual[1, 1], Is.EqualTo('e'));
            Assert.That(actual[1, 2], Is.EqualTo('f'));
        }

        private static IEnumerable<TestCaseData> CreateStructureCheckTestCases()
        {
            yield return Create("a", new char[,] { { 'a' } });
            yield return Create("ab", new char[,] { { 'a', 'b' } });
            yield return Create("a b", new char[,] { { 'a' }, { 'b' } });
            yield return Create("a b c", new char[,] { { 'a' }, { 'b' }, { 'c' } });
            yield return Create("ab cd ef", new char[,] { { 'a', 'b', }, { 'c', 'd' }, { 'e', 'f' } });
            yield return Create("123 456", new char[,] { { '1', '2', '3' }, { '4', '5', '6' } });

            yield return Create("1234 abcd GxfS", new char[,] {
                { '1', '2', '3', '4' },
                { 'a', 'b', 'c', 'd' },
                { 'G', 'x', 'f', 'S' },
            });

            // --------------------------------------------------------------------------------------
            TestCaseData Create(string input, char[,] expected)
            {
                return new TestCaseData(input, expected) { TestName = nameof(CreatesArrayCorrectly) + "For_" + input };
            }
        }
    }
}