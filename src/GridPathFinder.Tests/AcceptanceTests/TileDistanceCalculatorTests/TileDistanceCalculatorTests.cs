﻿using GridPathFinder.Tests._TestHelpers;
using GridPathFinder.Utilities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using static GridPathFinder.Tests.TestHelpers.TestCommon;

namespace GridPathFinder.Tests.AcceptanceTests.TileDistanceCalculatorTests
{
    [TestFixture]
    public sealed class TileDistanceCalculatorTests
    {
        [TestCaseSource(nameof(GetTestCases_FromDataFiles))]
        public void TestMethod(string input, string expectedOutput)
        {
            // Arrange
            char[,] array = input.To2DCharArray();
            List<Tile> tiles = new TwoDimensionalArrayInputConverter<char>().Convert(STdotHashFunc, array);

            // Act
            Dictionary<Tile, int> tileDistances = new TileDistanceCalculator(tiles).CalculateTileDistances();

            // Assert
            new VisualTileDistanceDictionaryChecker(tiles, tileDistances, expectedOutput)
                .Validate();
        }

        private static IEnumerable<TestCaseData> GetTestCases_FromDataFiles()
        {
            string testDataDirectoryPath = Path.Combine(
                TestContext.CurrentContext.WorkDirectory,
                nameof(AcceptanceTests),
                nameof(AcceptanceTests.TileDistanceCalculatorTests),
                "data");

            // Subdirectories
            string[] subDirectoriesPaths = Directory.GetDirectories(testDataDirectoryPath);
            foreach (string directoryPath in subDirectoriesPaths)
            {
                string dirName = Path.GetFileNameWithoutExtension(directoryPath);
                string[] subDirFilePaths = Directory.GetFiles(directoryPath);

                foreach (var item in ProcessFilePath(subDirFilePaths, dirName + "_"))
                    yield return item;
            }

            // Root level files
            string[] testFilePaths = Directory.GetFiles(testDataDirectoryPath);
            foreach (var item in ProcessFilePath(testFilePaths))
                yield return item;
        }

        private static IEnumerable<TestCaseData> ProcessFilePath(string[] testFilePaths, string dirNamePrefix = "")
        {
            foreach (string path in testFilePaths)
            {
                string testName = dirNamePrefix + Path.GetFileNameWithoutExtension(path);

                string fileContent = File.ReadAllText(path);
                if (fileContent.StartsWith("<< IGNORE >>" + Environment.NewLine))
                {
                    var testCaseData = new TestCaseData() { TestName = testName, };
                    testCaseData.Ignore($"Ignored by user (see '{Path.GetFileName(path)}')");
                    yield return testCaseData;
                }
                else
                {
                    string[] parts = fileContent.Split(new string[] { Environment.NewLine + Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                    Assert.That(parts.Length, Is.EqualTo(2), $"Error when parsing data file '{testName}', exactly 2 main parts expected");

                    yield return new TestCaseData(parts[0], parts[1]) { TestName = testName };
                }
            }
        }
    }
}