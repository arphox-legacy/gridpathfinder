﻿using NUnit.Framework;
using System;

namespace GridPathFinder.Tests.TileCollectionValidatorInput
{
    [TestFixture]
    public sealed class TileBasicTests
    {
        [TestCase(123, 456, TileType.Empty)]
        [TestCase(123, -456, TileType.Self)]
        [TestCase(-123, 456, TileType.Target)]
        [TestCase(-123, -456, TileType.Wall)]
        public void Ctor_SetsUpValuesCorrectly(int row, int column, TileType type)
        {
            var tile = new Tile(row, column, type);

            Assert.Multiple(() =>
            {
                Assert.That(tile.Row, Is.EqualTo(row));
                Assert.That(tile.Column, Is.EqualTo(column));
                Assert.That(tile.Type, Is.EqualTo(type));
            });
        }

        [Test]
        public void Ctor_ThrowsException_WhenNeeded()
        {
            Action action = () => new Tile(-1, -31, (TileType)int.MaxValue);

            Assert.Throws<ArgumentOutOfRangeException>(() => action());
        }

        [Test]
        public void Ctor_DoesNotThrowException_IfNotNeeded()
        {
            Action action = () => new Tile(-1, -31, TileType.Self);

            Assert.DoesNotThrow(() => action());
        }
    }
}