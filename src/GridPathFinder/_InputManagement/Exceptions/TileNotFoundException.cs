﻿using System;

namespace GridPathFinder.Exceptions
{
    public sealed class TileNotFoundException : ApplicationException
    {
        public TileNotFoundException(string message)
            : base(message)
        { }
    }
}