﻿namespace GridPathFinder
{
    public enum TileType
    {
        Empty = 0,
        Wall = 1,
        Target = 2,
        Self = 3
    }
}