﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace GridPathFinder.Utilities
{
    public static class EnumerableExtensions
    {
        [ExcludeFromCodeCoverage] // it is so simple it does not need any tests
        public static void ForEach<T>(this IEnumerable<T> enumeration, Action<T> action)
        {
            foreach (T item in enumeration)
                action(item);
        }
    }
}